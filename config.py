from lbconfig.api import *
#-----------------------------------------------------------------------------#
# Configuration
#-----------------------------------------------------------------------------#
relax_service_home = os.getenv("LB_RELAX_SERVICE")
measure_service_home= os.getenv("LB_MEASURE_SERVICE_HOME")
name='training'
name_ws=['data_node', 'frontend_node']
lbconfig_package(
 name=name,
 version='0.1', 
 default_prefix='out',
 default_targets=['lb-libraries', 'archive-ws-data_node','archive-ws-frontend_node']
)
depends_on( 
 logicblox_dep, 
 lb_web_dep,
 measureBlox = {
 'default_path' : measure_service_home,
 'help' : 'The BloxWeb Measure Service.'
 },

 relax_service={
      'default_path':relax_service_home 
  }
)
target('workspaces', ['archive-ws-data_node','archive-ws-frontend_node'])
#-----------------------------------------------------------------------------#
# LogiQl libraries
#-----------------------------------------------------------------------------#
measure_modules = [
 'measure_dims',
 'measure_metrics'
]
core_modules = [
 'dims',
 'metrics',

]
services_modules = [
 'dims_tdx',
 'metrics_tdx'
]
ws_modules = [
 'data_node',
 'frontend_node'
]
register_modules = [
 'register_data_node',
 'register_frontend_node',
 'register_relax_service',


]
#-----------------------------------------------------------------------------#
# Dependency configuration that you should not have to mess with
#-----------------------------------------------------------------------------#
external_libs = {
 'lb_web' : '$(lb_web)',
 'lb_measure_service' : '$(measureBlox)/share/lb-measure-service',
 'lb_web_connectblox' : '$(lb_web)',
 'lb_measure_txn_proto': '$(measureBlox)/share/lb-measure-service',
 'relax_service': '$(relax_service)'
}
#--
#-----------------------------------------------------------------------------#
# compiling LogiQl libraries
#-----------------------------------------------------------------------------#
for lib_name in core_modules:
 lb_library( name=lib_name, srcdir='src/core', deps=external_libs )
for lib_name in services_modules:
 lb_library( name=lib_name, srcdir='src/services/tdx_services', deps=external_libs )
for lib_name in ws_modules:
 lb_library( name=lib_name, srcdir='src', deps=external_libs )
for lib_name in measure_modules:
 lb_library( name=lib_name, srcdir='src/services/protobuf_services', deps=external_libs )
for lib_name in register_modules:
 lb_library( name=lib_name, srcdir='src/services', deps=external_libs )
#-----------------------------------------------------------------------------#
# Workspace
#-----------------------------------------------------------------------------#
ws_archive(name="data_node", libraries=['data_node'])
ws_archive(name="frontend_node", libraries=['frontend_node'])
#-----------------------------------------------------------------------------#
# Install
#-----------------------------------------------------------------------------#
install_dir('$(build)/workspaces', 'workspaces')
install_file('$(measureBlox)/lib/java/handlers/lb-measure-service.jar', 'config/lib/java')
install_file('$(relax_service)/lib/java/lb-relax-service.jar', 'config/lib/java')
install_dir('config', 'config')
#-----------------------------------------------------------------------------#
# Deploy
#-----------------------------------------------------------------------------#
rule(
 output='deploy',
 input=[],
 commands=[ 'sh script/deploy_data_workspace.sh ' ]
)
rule(
 output='deploymodel',
 input=[],
 commands=[ 'sh script/deploymodel.sh ' ]
 )
rule(
  output = "load-services",
  input=[],
  phony= True,
  commands=['lb web-server unload-services', 'lb web-server load -c out/config/lb-web-server.config'
,  'lb web-server load-services']
)
rule(
 output='deploy_frontend',
 input=[],
 commands=[ 'sh script/deploy_frontend_workspace.sh ' ]
)
rule(
 output='load_data',
 input=[],
 commands=[ 'sh script/load_data.sh ' ]
)
rule(
  output = "compile",
  input=[],
  phony= True,
  commands=["make && make install && make deploy && make deploy_frontend && make load-services && make load_data "]
)


 