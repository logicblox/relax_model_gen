
#!/bin/bash
export APP_HOME=$PWD
export LB_COMPONENTS=${APP_HOME}/upstream/logicblox
export PATH=/usr/lib/jvm/java-8-openjdk-amd64/jre/bin:$PATH
export PATH=/usr/lib/jvm/java-8-openjdk-amd64/bin:$PATH
export LB_RELAX_SERVICE=${APP_HOME}/upstream/relaxservice/dist
source $LB_COMPONENTS/etc/profile.d/logicblox.sh
source $LB_COMPONENTS/etc/bash_completion.d/logicblox.sh
export LB_CONNECTBLOX_ENABLE_ADMIN=1


  
